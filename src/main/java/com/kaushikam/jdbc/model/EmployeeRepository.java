package com.kaushikam.jdbc.model;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository {
    void save(Employee persisted) throws Exception;
    Optional<Employee> find(Long id) throws Exception;
    List<Employee> findAll() throws Exception;
}
