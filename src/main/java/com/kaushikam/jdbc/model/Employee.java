package com.kaushikam.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
public class Employee {
    private Long id;
    private String name;
    private Address address;
}
