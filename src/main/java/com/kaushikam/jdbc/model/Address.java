package com.kaushikam.jdbc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class Address {
    private String houseName;
    private String street;
    private String city;
    private String state;
}
