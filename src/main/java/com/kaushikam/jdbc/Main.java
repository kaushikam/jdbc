package com.kaushikam.jdbc;

import com.kaushikam.jdbc.infrastructure.impl.jdbc.EmployeeRepositoryJDBC;
import com.kaushikam.jdbc.model.Address;
import com.kaushikam.jdbc.model.Employee;
import com.kaushikam.jdbc.model.EmployeeRepository;

import java.sql.Connection;
import java.sql.DriverManager;

public class Main {
    static final String jdbcUrl = "jdbc:hsqldb:file:/home/kaushik/db-data/employee";
    static final String user = "SA";
    static final String password = "";

    public static void main(String[] args) throws Exception {
        initDb();

        Employee employee = new Employee(
                new Integer(1).longValue(),
                "Test-Emplyee",
                new Address(
                        "test house",
                        "Puthiyakavu",
                        "Ernakulam",
                        "Kerala"
                )
        );
        EmployeeRepository repository = new EmployeeRepositoryJDBC(jdbcUrl, user, password);
        repository.save(employee);

        Employee currentEmployee = repository.find(new Integer(1).longValue()).orElseThrow(UnknownError::new);
        System.out.println(currentEmployee);
    }

    private static void initDb() throws Exception {
        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        Connection connection = DriverManager.getConnection(jdbcUrl + ";ifexists=false", user, password);
        String sql = "CREATE TABLE IF NOT EXISTS employee(id INTEGER, name VARCHAR(50), house VARCHAR(50), street VARCHAR(50), city VARCHAR(50), state VARCHAR(50))";
        connection.createStatement().execute(sql);
    }
}
