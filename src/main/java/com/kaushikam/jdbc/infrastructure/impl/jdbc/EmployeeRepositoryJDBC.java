package com.kaushikam.jdbc.infrastructure.impl.jdbc;

import com.kaushikam.jdbc.model.Address;
import com.kaushikam.jdbc.model.Employee;
import com.kaushikam.jdbc.model.EmployeeRepository;
import lombok.AllArgsConstructor;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class EmployeeRepositoryJDBC implements EmployeeRepository {

    private String jdbcURL, user, password;

    @Override
    public void save(Employee persisted) throws Exception {
        Connection connection = getConnection();
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO employee VALUES (?, ?, ?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setLong(1, persisted.getId());
            statement.setString(2, persisted.getName());
            statement.setString(3, persisted.getAddress().getHouseName());
            statement.setString(4, persisted.getAddress().getStreet());
            statement.setString(5, persisted.getAddress().getState());
            statement.setString(6, persisted.getAddress().getState());

            int numOfRowsAffected = statement.executeUpdate();
            if (numOfRowsAffected <= 0)
                System.out.println("Employee details cannot be persisted!");
            else
                System.out.println("Employee details saved successfully");
        } catch (SQLException e) {
            e.printStackTrace();
            closeAll(null, statement, connection);
        } finally {
            closeAll(null, statement, connection);
        }
    }

    @Override
    public Optional<Employee> find(Long id) throws Exception {
        Connection connection = getConnection();
        String sql = "SELECT * FROM employee WHERE id = ?";
        PreparedStatement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                Employee employee = new Employee(rs.getLong("id"), rs.getString("name"),
                    new Address(rs.getString("house"), rs.getString("street"),
                            rs.getString("city"), rs.getString("state")));
                return Optional.of(employee);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            closeAll(rs, statement, connection);
            return Optional.empty();
        } finally {
            closeAll(rs, statement, connection);
        }
    }

    @Override
    public List<Employee> findAll() throws Exception {
        Connection connection = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        String sql = "SELECT * FROM employee";
        List<Employee> employees = new ArrayList<>();

        try {
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();

            while (rs.next()) {
                Employee employee = new Employee(rs.getLong("id"), rs.getString("name"),
                        new Address(rs.getString("house"), rs.getString("street"),
                                rs.getString("city"), rs.getString("state")));
               employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            closeAll(rs, statement, connection);
        } finally {
            closeAll(rs, statement, connection);
        }

        return employees;
    }

    private void closeAll(ResultSet rs, Statement st, Connection conn) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private Connection getConnection() throws Exception {
        Connection connection = null;

        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw e;
        }

        try {
            connection = DriverManager.getConnection(jdbcURL, user, password);
            return connection;
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
